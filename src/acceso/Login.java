package acceso;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import paginaInicio.PaginaInicio;

public class Login extends JFrame {
	PaginaInicio paginaInicio;

	JPanel panelLogin;
	String usuario[], contrasenya[];
	JTextField campoUsuario;
	JPasswordField campoPassword;
	JLabel etiquetaUsuario, etiquetaPassword, welcome, logo;
	JButton acceder;
	byte contador = 0;
	boolean acceso = false;

	public Login() {
		this.setTitle("Ingreso");
		this.setSize(1024, 768);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		inicioPanel();

		usuario = new String[2];
		contrasenya = new String[2];
		inicioEtiquetas();
		inicioUsuario();

		comprobarCredenciales();

	}

	private void inicioPanel() {

		panelLogin = new JPanel();
		panelLogin.setLayout(null);
		panelLogin.setBackground(new Color(253, 220, 145));

		this.getContentPane().add(panelLogin);

	}

	private void inicioEtiquetas() {
		welcome = new JLabel("Bienvenido. Por favor, introduce tu usuario y contraseña");

		logo = new JLabel();

		logo.setBounds(500, 100, 600, 600);
		logo.setIcon(new ImageIcon("logo.png"));

		panelLogin.add(logo);

		etiquetaUsuario = new JLabel("Usuario");
		etiquetaPassword = new JLabel("Password");

		welcome.setBounds(120, 170, 400, 25);
		panelLogin.add(welcome);

		etiquetaUsuario.setBounds(70, 280, 150, 25);
		panelLogin.add(etiquetaUsuario);

		etiquetaPassword.setBounds(70, 320, 150, 25);
		panelLogin.add(etiquetaPassword);

	}

	private void inicioUsuario() {
		campoUsuario = new JTextField();
		campoPassword = new JPasswordField();

		campoUsuario.setBackground(Color.WHITE);
		campoUsuario.setBounds(170, 280, 150, 25);
		panelLogin.add(campoUsuario);

		campoPassword.setBackground(Color.WHITE);
		campoPassword.setBounds(170, 320, 150, 25);
		panelLogin.add(campoPassword);

	}

	private void comprobarCredenciales() {

		acceder = new JButton("Login");
		acceder.setBounds(220, 370, 80, 25);
		panelLogin.add(acceder);

		usuario[0] = "root";

		contrasenya[0] = "root";

		usuario[1] = "usuario";
		contrasenya[1] = "contraseña";

		acceder.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String s = "";
				for (int i = 0; i < campoPassword.getPassword().length; i++) {
					s += campoPassword.getPassword()[i];

				}
				for (int j = 0; j < usuario.length; j++) {

					if (usuario[j].equals(campoUsuario.getText()) && contrasenya[j].equals(s)) {
						acceso = true;
						paginaInicio = new PaginaInicio();
						paginaInicio.setVisible(true);
						dispose();
					}
				}

				if (!acceso) {
					contador++;
					JOptionPane.showMessageDialog(null, "Intento " + contador + " Fallo",
							"Usuario y contraseña incorrectos", +JOptionPane.WARNING_MESSAGE);
					if (contador == 3) {
						JOptionPane.showMessageDialog(null,
								"Has sobrepasado el número de intentos. Inténtalo más tarde",
								"Número de intentos sobrepasado", +JOptionPane.WARNING_MESSAGE);
						System.exit(0);
					}
				}
				campoUsuario.setText("");
				campoPassword.setText("");
			}
		});
	}

}
