package paginaInicio;

import javax.swing.*;

import java.awt.*;

public class PaginaInicio extends JFrame {
	private JPanel panelInicio, panelLogo, panelImagen, panelTexto, panelMenu,panelPresentacion;
	private JMenuBar menubar;
	private JMenu[] menu;
	private JMenuItem[] menuComponentes, menuPerifericos, menuPortatiles, menuOrdenadores, menuConsolas;
	private JLabel logo, ordenador,texto1,texto2,texto3,texto;
	

	public PaginaInicio() {
		this.setTitle("Productos");
		this.setSize(1024, 768);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		inicioPanel();
		
		inicioItemsMenu();
	}

	private void inicioPanel() {
		panelInicio = new JPanel();
		panelInicio.setLayout(new BoxLayout(panelInicio,BoxLayout.Y_AXIS));
		panelInicio.setBackground(new Color(253, 220, 145));
		this.getContentPane().add(panelInicio);
		logo = new JLabel();
		logo.setIcon(new ImageIcon("logo2.png"));
		panelLogo=new JPanel();
		panelLogo.setBackground(new Color(253, 220, 145));
		panelLogo.setLocation(0,0);
		panelLogo.add(logo);
		ordenador=new JLabel();
		ordenador.setBackground(new Color(253, 220, 145));
		panelInicio.add(panelLogo);
		
		inicioMenu();
		panelPresentacion=new JPanel();
		panelPresentacion.setLayout(new BoxLayout(panelPresentacion,BoxLayout.X_AXIS));
		panelPresentacion.setBackground(new Color(253, 220, 145));
		panelImagen=new JPanel();
		panelImagen.setBackground(new Color(253, 220, 145));
		ordenador.setIcon(new ImageIcon("ordenador.jpg"));
		panelImagen.add(ordenador);
		panelPresentacion.add(panelImagen);
		
		
		texto=new JLabel();
		texto.setBackground(new Color(253, 220, 145));
		
		texto.setLayout(new BoxLayout(texto,BoxLayout.Y_AXIS));
		texto1 =new JLabel("TUS NECESIDADES SON NUESTRA PASIÓN");
		texto1.setBackground(new Color(253, 220, 145));
		texto.add(texto1);
		
		texto2=new JLabel("Te asesoramos y convertimos en realidad tus sueños.");
		texto2.setBackground(new Color(253, 220, 145));
		texto.add(texto2);
		
		texto3=new JLabel("Cualquier ordenador, consola o periférico es nuestro reto que afrontamos "
				+ "con la misma ilusión que tú.");
		texto3.setBackground(new Color(253, 220, 145));
		texto.add(texto3);
		panelPresentacion.add(texto);
		panelInicio.add(panelPresentacion);

	}

	private void inicioMenu() {
		panelMenu=new JPanel();
		panelMenu.setLocation(100,100);
		
		menubar = new JMenuBar();
		panelMenu.add(menubar);
		panelInicio.add(menubar);
		this.setJMenuBar(menubar);
		menu = new JMenu[5];
		menu[0] = new JMenu("Componentes");
		menu[1] = new JMenu("Periféricos");
		menu[2] = new JMenu("Portátiles");
		menu[3] = new JMenu("Ordenadores");
		menu[4] = new JMenu("Consolas");

		for (JMenu unMenu : menu) {
			menubar.add(unMenu);
		}

	}

	private void inicioItemsMenu() {
		menuComponentes = new JMenuItem[5];
		menuPerifericos = new JMenuItem[6];
		menuPortatiles = new JMenuItem[2];
		menuOrdenadores = new JMenuItem[5];
		menuConsolas = new JMenuItem[3];

		// ***Componentes***
		menuComponentes[0] = new JMenuItem("Placas base");
		menuComponentes[1] = new JMenuItem("Procesadores");
		menuComponentes[2] = new JMenuItem("Discos duros");
		menuComponentes[3] = new JMenuItem("Memoria");
		menuComponentes[4] = new JMenuItem("Tarjetas Gráficas");

		for (JMenuItem unItem : menuComponentes) {
			menu[0].add(unItem);
		}

		// ***Periféricos***
		menuPerifericos[0] = new JMenuItem("Monitores");
		menuPerifericos[1] = new JMenuItem("Teclados");
		menuPerifericos[2] = new JMenuItem("Ratones");
		menuPerifericos[3] = new JMenuItem("Altavoces");
		menuPerifericos[4] = new JMenuItem("Impresoras");
		menuPerifericos[5] = new JMenuItem("Escáneres");

		for (JMenuItem unItem : menuPerifericos) {
			menu[1].add(unItem);
		}

		// ***Portátiles***
		menuPortatiles[0] = new JMenuItem("Convertibles");
		menuPortatiles[1] = new JMenuItem("Mini PC");

		for (JMenuItem unItem : menuPortatiles) {
			menu[2].add(unItem);
		}

		// ***Ordenadores***
		menuOrdenadores[0] = new JMenuItem("Gaming");
		menuOrdenadores[1] = new JMenuItem("Servidores");
		menuOrdenadores[2] = new JMenuItem("ALL-IN-ONE");
		menuOrdenadores[3] = new JMenuItem("Oficina");
		menuOrdenadores[4] = new JMenuItem("A medida");

		for (JMenuItem unItem : menuOrdenadores) {
			menu[3].add(unItem);
		}

		// ***Consolas***
		menuConsolas[0] = new JMenuItem("Retro");
		menuConsolas[1] = new JMenuItem("Portátiles");
		menuConsolas[2] = new JMenuItem("Sobremesa");

		for (JMenuItem unItem : menuConsolas) {
			menu[4].add(unItem);
		}

	}
}